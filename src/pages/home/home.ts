import { Component } from '@angular/core';
import { NavController, App, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { PengaturanPage } from '../pengaturan/pengaturan';
import { TransferPage } from '../transfer/transfer';
import { RegisterPage } from '../register/register';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ReceiveBalancePage } from '../receive-balance/receive-balance';
import { UserLogProvider } from '../../providers/user-log/user-log';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public registerPage = RegisterPage
  public Pengaturan = PengaturanPage
  public transfer = TransferPage
  private baseURI : string  = "https://recehin.herokuapp.com/";
  public currentUser: any;

  constructor(
    public navCtrl: NavController,
    public appCtrl: App,
    private barcodeScanner: BarcodeScanner,
    public http   : HttpClient,
    public alertCtrl: AlertController,
    public loadingCtl : LoadingController,
    public userLog: UserLogProvider,
    private toastCtrl: ToastController) 
    {
      this.currentUser = this.userLog.curruentUser;
    }

  ionViewDidLoad() {
    
  }

  ionViewWillEnter(){
    this.currentUser = this.userLog.curruentUser;
  }

  scanCode() {
    this.barcodeScanner.scan().then(barcodeData => {

    let loading = this.loadingCtl.create({
      content: 'Adding Balance'
    });

    loading.present().then(()=>{
      let headers 	: any		= new HttpHeaders({ 'Content-Type': 'application/json' }),
          options 	: any		= {
            user_id: this.currentUser.id,
            balance: barcodeData.text
          },
          url       : any      	= this.baseURI + "add/balance";

          this.http.post(url, JSON.stringify(options), headers)
          .subscribe((data : any) =>
          {
            loading.dismiss();
            this.userLog.curruentUser = data.user
            this.currentUser = data.user

            let alert = this.alertCtrl.create({
              title: data.message,
              buttons: ['OK']
            });
            alert.present();
            console.log(data);
          },
          (error : any) =>
          {
            console.log(error);
            loading.dismiss();
            let toast = this.toastCtrl.create({
              message: 'Cant add balance, Some error has occured',
              duration: 3000,
              position: 'bottom'
            });
            toast.present();
          });
    });

    }, (err) => {
        console.log('Error: ', err);
    });
  }
  goSetting() {
    this.appCtrl.getRootNav().push(this.Pengaturan)
  }

  goTransfer() {
    this.appCtrl.getRootNav().push(this.transfer)
  }

  showRegister(){
    this.appCtrl.getRootNav().push(this.registerPage)
  }


}
