import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  public fullname: any;
  public phoneNumber: any;
  public email: any;
  public securityCode: any;
  private baseURI : string  = "https://recehin.herokuapp.com/";

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public http   : HttpClient) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  register(){
    let headers 	: any		= new HttpHeaders({ 'Content-Type': 'application/json' }),
          options 	: any		= {
            fullname: this.fullname,
            phone_number: this.phoneNumber,
            email: this.email
          },
          url       : any      	= this.baseURI + "register";

    this.http.post(url, JSON.stringify(options), headers)
      .subscribe((data : any) =>
      {
        console.log(data);
      },
      (error : any) =>
      {
        console.log(error);
      });
  }

}
