import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {DomSanitizer} from '@angular/platform-browser';
import { UserLogProvider } from '../../providers/user-log/user-log';

/**
 * Generated class for the DealsDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-deals-detail',
  templateUrl: 'deals-detail.html',
})
export class DealsDetailPage {
  public voucher: any;
  public banner: any;
  public currentUser: any;

  private baseURI : string  = "https://recehin.herokuapp.com/";

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public loadingCtl : LoadingController, 
    public http: HttpClient,
    private sanitizer:DomSanitizer,
    public userLog: UserLogProvider,
    private toastCtrl: ToastController) 
  {
      this.currentUser = this.userLog.curruentUser;
      console.log(this.userLog.curruentUser)
      this.voucher = navParams.get('voucher');
      console.log(this.voucher.name)
      this.banner = this.sanitizer.bypassSecurityTrustStyle(`url(${this.voucher.banner})`);
      
  }


  ionViewDidLoad(){
  }

  buyVoucher(){

    let loading = this.loadingCtl.create({
      content: 'Taking it'
    });

    loading.present().then(()=>{
      let headers 	: any		= new HttpHeaders({ 'Content-Type': 'application/json' }),
          options 	: any		= {
            user_id: this.currentUser.id,
            voucher_id: this.voucher.id
          },
          url       : any   = this.baseURI + "buy/voucher";


      this.http
          .post(url, JSON.stringify(options), headers)
          .subscribe(
            (data : any) => {
              console.log(data);
              this.userLog.curruentUser = data.user
              this.currentUser = data.user
              this.voucher = data.voucher
              loading.dismiss();
              let toast = this.toastCtrl.create({
                message: data.message,
                duration: 3000,
                position: 'bottom'
              });
              toast.present();
              
            },
            (error : any) => {
              console.log(error);
              loading.dismiss();
              let toast = this.toastCtrl.create({
                message: 'Whoops, Some error just occured',
                duration: 3000,
                position: 'bottom'
              });
              toast.present();
            }
          );
    });


    

    
  }

}
