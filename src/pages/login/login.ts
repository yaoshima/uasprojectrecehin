import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, App, ToastController, LoadingController } from 'ionic-angular';
import { OtpPage } from '../otp/otp';
import { RegisterPage } from '../register/register';

import {Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserLogProvider } from '../../providers/user-log/user-log';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  public registerPage = RegisterPage
  mobile:any;
  linkotp:string;
  private baseURI : string  = "https://recehin.herokuapp.com/";
  constructor(
    public navCtrl: NavController, 
    public alertCtrl: AlertController,
    public http:Http, 
    public navParams: NavParams,
    public appCtrl: App, 
    public httpClient: HttpClient,
    private toastCtrl: ToastController,
    public userLog: UserLogProvider,
    public loadingCtl : LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  
  sendOTP()
  {
    if(this.mobile.length!=13)
      {
       let alert = this.alertCtrl.create({
          title: 'Mobile Number Required!',
          subTitle: 'Please enter your 10 digit mobile number with 91 country code!',
          buttons: ['OK']
        });
        alert.present();
      }
    else
      {
        console.log("ini MOBILE " + this.mobile);
        this.linkotp='http://bikin-film.com/nexmosms/send-sms.php?x='; //HARUS TARUH DI HTDOCS FOLDER NEXMOSMS
        console.log("ini linkotp sebelum " + this.linkotp);
        this.linkotp = this.linkotp + this.mobile;
        console.log("ini linkotp sesudah " + this.linkotp);
        this.http.get(this.linkotp)
        .map(res => res.json())
        .subscribe(res => {
          // alert(JSON.stringify(res));
          console.log("JSAFAHDIUH");
          
          }, (err) => {
            console.log("HANSEL");
            this.navCtrl.push(OtpPage,{mobileno:this.mobile});
          // alert("failed");
        });
      }
  }

  goRegister(){
    this.appCtrl.getRootNav().push(this.registerPage)
  }

  tryLogin(){
    let headers 	: any		= new HttpHeaders({ 'Content-Type': 'application/json' }),
          options 	: any		= {
            phone_number: this.mobile
          },
          url       : any      	= this.baseURI + "login";


    let loading = this.loadingCtl.create({
      content: 'Loggin in'
    });

    loading.present().then(()=>{
      this.httpClient
      .post(url, JSON.stringify(options), headers)
      .subscribe(
        (data : any) => {
          console.log(data);
          if(data.error == '0'){
            this.userLog.setCurrentUser(data.user);
            console.log('from user service')
            console.log(this.userLog.curruentUser)
            //this.sendOTP();
            this.navCtrl.push(OtpPage);
          }
          else{
            let toast = this.toastCtrl.create({
              message: data.message,
              duration: 3000,
              position: 'bottom'
            });
            toast.present();
          
          }
          loading.dismiss();
        },
        (error : any) => {
          loading.dismiss();
          let toast = this.toastCtrl.create({
            message: 'Cant log you in, Some error has occured',
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
          console.log(error);
        });
    });

    
  }

}
