import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Platform } from 'ionic-angular';
import { HomePage } from '../home/home';

import { TabsPage } from '../tabs/tabs';

import { Http } from '@angular/http';



/**
 * Generated class for the OtpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-otp',
  templateUrl: 'otp.html',
})
export class OtpPage {
  public otp:any;
  mobile='';
  constructor(public alertCtrl: AlertController,
    public platform:Platform,
    public http:Http,
    public navCtrl:NavController,
    public navParams: NavParams) {

    
    // document.addEventListener('onSMSArrive', function(e){
    // var sms = e.data;
      
    // console.log("received sms "+JSON.stringify( sms ) );
      
    // if(sms.address=='HP-611773') //look for your message address
    //     {
    //     this.otp=sms.body.substr(0,4);
    //     this.stopSMS();
    //     this.verify_otp();
    //     }
    //     });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OtpPage');
  }
  
    // ionViewDidLoad() {
    //   console.log('ionViewDidLoad OtpReceivePage');
    //   this.mobile=this.navParams.get('mobileno');
    //   console.log(this.mobile + "HAHA");
    //   this.checkPermission();
    // }
  

  goNext() {
    this.navCtrl.setRoot(TabsPage);
  }

}
