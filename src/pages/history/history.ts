import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { UserLogProvider } from '../../providers/user-log/user-log';

/**
 * Generated class for the HistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {

  public history: any[];
  public currentUser: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public loadingCtl : LoadingController, 
    public http: HttpClient, 
    public userLog: UserLogProvider,
    private toastCtrl: ToastController) 
  {
      this.currentUser = this.userLog.curruentUser;
  }

  load(){
    let loading = this.loadingCtl.create({
      content: 'Fetching Transaction'
    });

    loading.present().then(()=>{
      this.http
      .get('https://recehin.herokuapp.com/user/transaction/' +  this.currentUser.id)
      .subscribe((data : any) =>
      {
         console.dir(data);
         this.history = data;
         loading.dismiss();
      },
      (error : any) =>
      {
         console.dir(error);
         loading.dismiss();
         let toast = this.toastCtrl.create({
          message: 'Cant fetch history, Some error has occured',
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      });
    });

    
  }

  ionViewWillEnter() {
    this.load();
  }

}
