import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { DealsDetailPage } from '../deals-detail/deals-detail';

/**
 * Generated class for the DealsListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-deals-list',
  templateUrl: 'deals-list.html',
})
export class DealsListPage {

  public dealsDetailPage = DealsDetailPage;
  public dealsList: any[];

  constructor(public navCtrl: NavController, 
    public navParams  : NavParams, 
    public http       : HttpClient,
    public loadingCtl : LoadingController,
    private toastCtrl: ToastController
  ) {}

  ionViewDidLoad() {
    // this.dealsList = [
    //   {
    //     banner: 'assets/imgs/banner2.jpg',
    //     vendor: 'Anang Family Karoke',
    //     description: '2 jam karaoke (Small Room) + Snacks',
    //     price: 'Rp75.000',
    //     logo: 'assets/imgs/banner3.jpg'
    //   },
    //   {
    //     banner: 'assets/imgs/banner2.jpg',
    //     vendor: 'Anang Family Karoke',
    //     description: '2 jam karaoke (Small Room) + Snacks',
    //     price: 'Rp75.000',
    //     logo: 'assets/imgs/banner3.jpg'
    //   }
    // ]

    this.load();
  }

  load(){
    let loading = this.loadingCtl.create({
      content: 'Fetching Voucher'
    });

    loading.present().then(()=>{
      this.http
      .get('https://recehin.herokuapp.com/vouchers')
      .subscribe((data : any) =>
      {
         console.dir(data);
         this.dealsList = data;
         loading.dismiss();
      },
      (error : any) =>
      {
         console.dir(error);
         loading.dismiss();
         let toast = this.toastCtrl.create({
          message: 'Whoops, Some error just occured',
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      });
    });

    
  }


   getVoucher(voucher:any){
      this.navCtrl.push(this.dealsDetailPage,{voucher: voucher})
   }

}
