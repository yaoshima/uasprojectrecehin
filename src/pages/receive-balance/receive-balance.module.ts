import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReceiveBalancePage } from './receive-balance';

@NgModule({
  declarations: [
    ReceiveBalancePage,
  ],
  imports: [
    IonicPageModule.forChild(ReceiveBalancePage),
  ],
})
export class ReceiveBalancePageModule {}
