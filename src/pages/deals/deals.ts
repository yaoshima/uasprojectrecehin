import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { DealsDetailPage } from '../deals-detail/deals-detail';
import { DealsListPage } from '../deals-list/deals-list';

/**
 * Generated class for the DealsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-deals',
  templateUrl: 'deals.html',
})
export class DealsPage {
  public dealsDetail = DealsDetailPage
  public dealsList = DealsListPage

  constructor(public navCtrl: NavController, public navParams: NavParams,public appCtrl: App) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DealsPage');
  }

  featuredDeal(){
    this.appCtrl.getRootNav().push(this.dealsDetail)
  }

  getDealsDetail(){
    this.appCtrl.getRootNav().push(this.dealsList)
  }
}
