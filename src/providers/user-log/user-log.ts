import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the UserLogProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserLogProvider {
  public curruentUser: any;

  constructor(public http: HttpClient) {
    console.log('Hello UserLogProvider Provider');
  }

  setCurrentUser(user){
    this.curruentUser = user;
  }
  

}
