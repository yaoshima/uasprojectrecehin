import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonAffixModule} from 'ion-affix';

//import { AndroidPermissions } from '@ionic-native/android-permissions'; //OTP
import { HttpModule } from '@angular/http'; //OTP
import { HttpClientModule } from '@angular/common/http';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ExpandableHeaderComponent } from '../components/expandable-header/expandable-header';
import { DealsDetailPage } from '../pages/deals-detail/deals-detail';
import { DealsListPage } from '../pages/deals-list/deals-list';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { OtpPage } from '../pages/otp/otp';
import { TabsPage } from '../pages/tabs/tabs';
import { EditprofilePage } from '../pages/editprofile/editprofile';
import { TransferPage } from '../pages/transfer/transfer';
import { PengaturanPage } from '../pages/pengaturan/pengaturan';
import { UserLogProvider } from '../providers/user-log/user-log';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    DealsDetailPage,
    DealsListPage,
    ExpandableHeaderComponent,
    LoginPage,
    RegisterPage,
    OtpPage,
    TabsPage,
    PengaturanPage,
    EditprofilePage,
    TransferPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonAffixModule,
    HttpModule,
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    DealsDetailPage,
    DealsListPage,
    LoginPage,
    RegisterPage,
    OtpPage,
    TabsPage,
    PengaturanPage,
    EditprofilePage,
    TransferPage
  ],
  providers: [
    // AndroidPermissions,
    StatusBar,
    SplashScreen,
    BarcodeScanner,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserLogProvider
  ]
})
export class AppModule {}
